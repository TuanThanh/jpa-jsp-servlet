<%-- 
    Document   : test
    Created on : Dec 26, 2019, 4:57:07 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome, ${sessionScope.loginUser}</h1>
        <form action="Manage" method="POST">
            Employee ID : <input type="text" name="empid" value="${employee.empid}"/><br>
            First Name : <input type="text" name="firstname" value="${employee.firstname}"/><br>
            Last Name : <input type="text" name="lastname" value="${employee.lastname}"/><br>
            Email : <input type="text" name="email" value="${employee.email}"/><br>
            
            <input type="submit" name="action" value="ADD"/>|
            <input type="submit" name="action" value="EDIT"/>|
            <input type="submit" name="action" value="DELETE"/>|
            <input type="submit" name="action" value="VIEW"/>|
            
            <table>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                    <c:forEach var="emp" items="${getAllEmployee}">
                    <TR>
                        <td>${emp.empid}</td>
                        <td>${emp.firstname}</td>
                        <td>${emp.lastname}</td>
                        <td>${emp.email}</td>
                    </TR>
                    </c:forEach>
            </table>
        </form>
    </body>
</html>
